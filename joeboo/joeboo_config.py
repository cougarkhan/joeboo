# Import Modules
import os
import sys
import requests as r
import httputils
import cx_Oracle as cx
import mysql.connector
from ldap3 import Server, Connection, operation, SIMPLE, SYNC, ASYNC, SUBTREE, ALL
import logging
import time
import joeboo_config

class oracle_host(object):
        
    def __init__(self,uri,port,sid,username,password,query):
        self.name = "Galactica Actual"
        self.uri = uri
        self.port = port
        self.sid = sid
        self.dsn = cx.makedsn(uri,port,sid)
        self.query = query
        self.username = username
        self.password = password
        self.conn = cx.Connection
        self.result = None


    def connect(self):
        self.conn = cx.connect(self.username, self.password, self.dsn)

    def execute(self):
        cursor = self.conn.cursor()
        cursor.execute(self.query)
        self.result = cursor.fetchall()

class eldap_host:

    def __init__(self):
        self.name = "Cylon Basestar"